from django.db import models

# Create your models here.
from .validators import validate_sks
class Kuliah(models.Model):
    Mata_Kuliah       = models.CharField(max_length = 100)
    Dosen_Pengajar    = models.CharField(max_length = 100)
    Jumlah_SKS        = models.CharField(
        max_length = 10,
        validators = [validate_sks]
        )
    Deskripsi         = models.TextField()
    Ruang_Kelas       = models.CharField(max_length = 100)
    Semester_Tahun    = models.CharField(max_length = 100)
    def __str__(self):
        return "{}.{}".format(self.id, self.Mata_Kuliah)