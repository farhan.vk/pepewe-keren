from django.core.exceptions import ValidationError

def validate_sks(value):
    jumlah_sks = value
    if(not(jumlah_sks.isdigit())or jumlah_sks=="0"):
        message = "masukkan jumlah sks dengan benar"
        raise ValidationError(message)