from django.test import TestCase, Client
from django.http import HttpRequest
from . import views
from .models import Kegiatan, Peserta

# Create your tests here.
class TestKegiatan(TestCase):
    def test_url_kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_template_kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/index.html')

    def test_url_create(self):
        response = Client().get('/create/')
        self.assertEqual(response.status_code, 302)

    def test_tambah_kegiatan(self):
        response = Client().post('/create/', {'Nama_Kegiatan': 'Makan Bersama'})
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(response.status_code, 302)

    def test_tambah_peserta(self):
        kegiatan = Kegiatan.objects.create(Nama_Kegiatan="Makan Bersama")
        response = Client().post('/tambah_peserta/{kegiatan_id}', {'Nama_Peserta':'Ucok'})
        self.assertEqual(kegiatan.Daftar_Peserta.count(), 1)
        self.assertEqual(response.status_code, 302)
        
    def test_index_view(self):
        response = views.index(HttpRequest())
        self.assertEquals(response.status_code, 200)
        self.assertIn('Tambah Kegiatan', response.content.decode('utf8'))

    def test_create_view(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['Nama_Kegiatan'] = 'Makan Bersama'
        self.assertEquals(Kegiatan.objects.count(), 1)
        self.assertEquals(response.status_code, 302)

    def test_tambahPeserta_view(self):
        kegiatan = Kegiatan.objects.create(Nama_Kegiatan="Makan Bersama")
        request = HttpRequest()
        request.method = 'POST'
        request.POST['nama_peserta'] = 'Udin'
        kegiatan_id = kegiatan_id
        response = views.tambahPeserta(request, kegiatan_id)
        self.assertEquals(response.status_code, 302)

    def test_kegiatan_ada_peserta(self):
        kegiatan = Kegiatan.objects.create(Nama_Kegiatan="Makan Bersama")
        udin = Peserta.objects.create(Nama_Peserta="Udin")
        bambank = Peserta.objects.create(Nama_Peserta="Bambank")
        kegiatan.Daftar_Peserta.set([udin, bambank])
        self.assertEqual(kegiatan.Daftar_Peserta.count(), 2)
    
    def test_model_str(self):
        kegiatan = Kegiatan.objects.create(Nama_Kegiatan="Makan Bersama")
        udin = Peserta.objects.create(Nama_Peserta="Udin")
        self.assertEqual(str(kegiatan), "Makan Bersama")
        self.assertEqual(str(udin), "Udin")
