from django.shortcuts import render, redirect
from .forms import KegiatanForm, PesertaForm
from .models import Kegiatan, Peserta


def index(request):
    semua_kegiatan = Kegiatan.objects.all()

    context = {
        'semua_kegiatan':semua_kegiatan,
    }
    return render(request, 'kegiatan/index.html', context)

def create(request):
    kegiatan_form = KegiatanForm(request.POST or None)
    if request.method == 'POST':
        if kegiatan_form.is_valid():
            kegiatan_form.save()

            return redirect('kegiatan:index')
    
    context = {
        'kegiatan_form':kegiatan_form,
    }
    return redirect('kegiatan:index', context)

def tambahPeserta(request, kegiatan_id):
    peserta_form = PesertaForm(request.POST or None)
    kegiatan = Kegiatan.objects.get(id=kegiatan_id)
    if (request.method == 'POST'):
        if peserta_form.is_valid():
            peserta_form.save()

            return redirect('kegiatan:index')
