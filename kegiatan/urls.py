from django.urls import path

from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('', views.index, name='index'),
    path('create/', views.create, name='create'),
    path('tambahPeserta/<int:kegiatan_id>/', views.tambahPeserta, name='tambahPeserta'),
]