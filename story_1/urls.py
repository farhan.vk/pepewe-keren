from django.urls import path

from . import views

app_name = 'story_1'

urlpatterns = [
    path('', views.story_1, name='story_1'),
]
