from django.db import models

# Create your models here.
class Peserta(models.Model):
    Nama_Peserta = models.CharField(max_length=100)

    def __str__(self):
        return self.Nama_Peserta

class Kegiatan(models.Model):
    Nama_Kegiatan = models.CharField(max_length=100)
    Daftar_Peserta = models.ManyToManyField(to=Peserta)

    def __str__(self):
        return self.Nama_Kegiatan

