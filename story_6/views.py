from django.shortcuts import render, redirect

from .forms import KegiatanForm, PesertaForm
from .models import Kegiatan, Peserta


def index(request):
    semua_kegiatan = Kegiatan.objects.all()

    context = {
        'semua_kegiatan':semua_kegiatan,
    }

    return render(request, 'story_6/index.html', context)

def add_kegiatan(request):
    kegiatan_form = KegiatanForm(request.POST or None)
    if request.method == 'POST':
        if kegiatan_form.is_valid():
            kegiatan_form.save()

            return redirect('story_6:index')
    
    context = {
        'kegiatan_form':kegiatan_form,
    }
    return render(request, 'story_6/add_kegiatan.html', context)

def add_peserta(request, kegiatan_id):
    
    peserta_form = PesertaForm(request.POST or None)
    if request.method == 'POST':
        if peserta_form.is_valid():
            kegiatan = Kegiatan.objects.get(id=kegiatan_id)
            kegiatan.Daftar_Peserta.add(peserta_form)
            return redirect('story_6:index')
    
    context = {
        'peserta_form':peserta_form,
    }
    return render(request, 'story_6/add_peserta.html', context)
