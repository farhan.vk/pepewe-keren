from django.urls import path

from . import views

app_name = 'story_6'

urlpatterns = [
    path('', views.index, name='index'),
    path('add_kegiatan/', views.add_kegiatan, name='add_kegiatan'),
    path('add_peserta/<int:kegiatan_id>/', views.add_peserta, name='add_peserta'),
]
